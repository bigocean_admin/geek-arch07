package com.ocean.test;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import java.util.List;
import java.util.concurrent.*;

/**
 * 程序入口
 */
public class Starter {

    /**
     * 程序入口
     * @param args
     */
    public static void main(String[] args) {
        WebTestResult result = test("https://www.baidu.com",10,100);
        System.out.println(result);
    }

    /**
     * 测试方法
     * @param url 测试地址
     * @param userCount 用户数量
     * @param reqCount 每个用户访问多少次
     */
    public static WebTestResult test(String url,int userCount,int reqCount){
        ExecutorService executorService = Executors.newFixedThreadPool(userCount);
        CompletionService<List<Integer>> cs = new ExecutorCompletionService<List<Integer>>(executorService);
        for (int i = 0; i < userCount; i++){
            cs.submit(new WebTestCase(url,reqCount));
        }
        double[] timesList = new double[userCount * reqCount];
        int index = 0;
        for (int i = 0; i < userCount; i++) {
            try {
                List<Integer> list = cs.take().get();
                for(Integer time : list){
                    timesList[index] = time.doubleValue();
                    index = index + 1;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        Percentile percentile = new Percentile(0.95);
        double nintyFivepercentile = percentile.evaluate(timesList);
        Mean mean = new Mean(); // 算术平均值
        double average = mean.evaluate(timesList);
        WebTestResult result = new WebTestResult(average,nintyFivepercentile);
        return result;
    }
}
