package com.ocean.test;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * WEB测试用例
 */
public class WebTestCase implements Callable<List<Integer>> {
    /**
     * 接口URL
     */
    private String url;

    /**
     * 每个用户请求数量
     */
    private int reqCount;

    public WebTestCase(String url,int reqCount) {
        this.url = url;
        this.reqCount = reqCount;
    }

    public List<Integer> call() throws Exception {
        List<Integer> spendTimes = new ArrayList<Integer>();
        for(int i = 0;i< reqCount;i++){
            Long startMills = System.currentTimeMillis();
            String result =  httpRequest(url,"GET",null);
            Long endMills = System.currentTimeMillis();
            Long spendTime = endMills - startMills;
            spendTimes.add(spendTime.intValue());
            System.out.println("线程"+Thread.currentThread().getName()+"第"+(i+1)+"次:"+spendTime.intValue()+"ms");
        }
        return spendTimes;
    }

    public static String httpRequest(String requestUrl, String requestMethod, String outputStr) {
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection)url.openConnection();
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setDoInput(true);
            httpUrlConn.setUseCaches(false);
            httpUrlConn.setConnectTimeout(500);
            httpUrlConn.setRequestMethod(requestMethod);
            if ("GET".equalsIgnoreCase(requestMethod))
                httpUrlConn.connect();
            if (outputStr != null) {
                OutputStream outputStream = httpUrlConn.getOutputStream();
                outputStream.write(outputStr.getBytes("UTF-8"));
                outputStream.close();
            }
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String str = null;
            while ((str = bufferedReader.readLine()) != null){
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            inputStream = null;
            httpUrlConn.disconnect();
        } catch (ConnectException ce) {
            ce.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }
}
