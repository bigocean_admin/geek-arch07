package com.ocean.test;

/**
 * 测试结果
 */
public class WebTestResult {
    /**
     * 平均响应时间
     */
    private Double avgRespMills;

    /**
     * 95%响应时间
     */
    private Double nintyFivePercentileRespMills;

    public WebTestResult(Double avgRespMills, Double nintyFivePercentileRespMills) {
        this.avgRespMills = avgRespMills;
        this.nintyFivePercentileRespMills = nintyFivePercentileRespMills;
    }

    @Override
    public String toString() {
        return "WebTestResult{" +
                "avgRespMills=" + avgRespMills +
                ", nintyFivePercentileRespMills=" + nintyFivePercentileRespMills +
                '}';
    }
}
